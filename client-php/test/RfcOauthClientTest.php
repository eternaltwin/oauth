<?php declare(strict_types=1);

namespace Eternaltwin\OauthClient\Test;

use Eternaltwin\OauthClient\RfcOauthClient;
use PHPUnit\Framework\TestCase;

final class RfcOauthClientTest extends TestCase {
  public function testGetAuthorizationUri(): void {
    $client = new RfcOauthClient(
      "https://eternal-twin.net/oauth/authorize",
      "https://eternal-twin.net/oauth/token",
      "https://mygame.localhost/oauth/callback",
      "mygame@clients",
      "dev_secret"
    );

    $actual = $client->getAuthorizationUri("base", "authenticate")->__toString();
    // TODO: Escape query parameters!
    $expected = "https://eternal-twin.net/oauth/authorize?access_type=offline&response_type=code&redirect_uri=https://mygame.localhost/oauth/callback&client_id=mygame@clients&scope=base&state=authenticate";

    $this->assertEquals($expected, $actual);
  }
}
