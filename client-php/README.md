# PHP client for OAuth

[![packagist](https://img.shields.io/packagist/v/eternaltwin/oauth-client)][packagist]

## Usage

```
composer require eternaltwin/oauth-client
```

1. Create the OAuth client

   ```php
   use Eternaltwin\OauthClient\RfcOauthClient;

   // See https://eternal-twin.net/docs/app/config
   $CONFIG = ...;

   $OAUTH_CLIENT = new RfcOauthClient(
     // Authorization endpoint
     $CONFIG->getEternaltwinUri() . "/oauth/authorize",
     // Token endpoint
     $CONFIG->getEternaltwinUri() . "/oauth/token",
     // Callback endpoint
     $CONFIG->getExternalUri() . "/callback.php",
     // Client id
     $CONFIG->getEternaltwinClientId(),
     // Client secret
     $CONFIG->getEternaltwinClientSecret(),
   );
   ```

2. When you need to authenticate the user, initiate the OAuth protocol with a
   redirection:

   ```php
   // There are no real OAuth scopes yet, use `base`
   $scopes = "base";
   // Use the state to remember how the authentication was initiated.
   // This example uses a simple string, it is recommended to use a signed JWT.
   $state = "sign_in";

   $authorizationUri = $OAUTH_CLIENT->getAuthorizationUri($scope, $state);
   header("Location: " . $authorizationUri, true, 302);
   exit;
   ```

3. In your OAuth callback handler (e.g. `callback.php`), handle the user return
   and claim the access token.

   ```php
   $code = $_GET["code"];
   $state = $_GET["state"];
   $accessToken = $OAUTH_CLIENT->getAccessTokenSync($code);
   ```

4. Next steps: once you have the access token, you can use it with [the API
   client](https://gitlab.com/eternal-twin/etwin/-/tree/master/clients/php) to
   retrieve more data about the current user.

   ```
   use \Etwin\Client\Auth;
   use \Etwin\Client\HttpEtwinClient;
   use \Etwin\User\UserId;

   $apiClient = new HttpEtwinClient($CONFIG->getEtwinUri());
   $self = $client->getSelf(Auth::fromToken($accessToken->getAccessToken()));

   // var_dump($self);

   // $user = $self->getUser();
   // $displayName = $user->getDisplayName()->getCurrent()->getInner();
   // $userId = $user->getId();
   ```

## Contributing

```
composer install
composer test
```

## Publish

```
composer run-script publish
```

[packagist]: https://packagist.org/packages/eternaltwin/oauth-client
