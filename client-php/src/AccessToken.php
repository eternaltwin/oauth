<?php declare(strict_types=1);

namespace Eternaltwin\OauthClient;

final class AccessToken implements \JsonSerializable {
  private string $accessToken;
  private ?string $refreshToken;
  private int $expiresIn;
  private TokenType $tokenType;

  final public function __construct(
    string $acessToken,
    ?string $refreshToken,
    int $expiresIn,
    TokenType $tokenType
  ) {
    $this->accessToken = $acessToken;
    $this->refreshToken = $refreshToken;
    $this->expiresIn = $expiresIn;
    $this->tokenType = $tokenType;
  }

  final public function getAccessToken(): string {
    return $this->accessToken;
  }

  final public function getRefreshToken(): ?string {
    return $this->refreshToken;
  }

  final public function getExpiresIn(): int {
    return $this->expiresIn;
  }

  final public function getTokenType(): TokenType {
    return $this->tokenType;
  }

  final public function jsonSerialize(): array {
    return [
      "access_token" => $this->accessToken,
      "refresh_token" => $this->refreshToken,
      "expires_in" => $this->expiresIn,
      "token_type" => $this->tokenType->jsonSerialize(),
    ];
  }

  /**
   * @param mixed $raw
   * @return self
   */
  final public static function jsonDeserialize($raw): self {
    $accessToken = $raw["access_token"];
    $refreshToken = isset($raw["refresh_token"]) ? $raw["refresh_token"] : null;
    $expiresIn = $raw["expires_in"];
    $tokenType = TokenType::jsonDeserialize($raw["token_type"]);
    return new self($accessToken, $refreshToken, $expiresIn, $tokenType);
  }

  /**
   * @param string $json
   * @return self
   * @throws \JsonException
   */
  final public static function fromJson(string $json): self {
    return self::jsonDeserialize(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
  }
}
