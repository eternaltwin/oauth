package net.eternaltwin.oauth.client

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString

@Serializable
data class OauthAccessTokenRequest(
  @SerialName("client_id")
  val clientId: String,
  @SerialName("client_secret")
  val clientSecret: String,
  @SerialName("redirect_uri")
  val redirectUri: String,
  val code: String,
  @SerialName("grant_type")
  val grantType: String,
) {
  companion object {
    @JvmStatic
    fun fromJsonString(jsonString: String): OauthAccessTokenRequest = JSON_FORMAT.decodeFromString(jsonString)

    @JvmStatic
    fun toJsonString(value: OauthAccessTokenRequest): String = JSON_FORMAT.encodeToString(value)
  }
}
