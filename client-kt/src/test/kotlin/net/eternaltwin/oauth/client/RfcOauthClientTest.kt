package net.eternaltwin.oauth.client;

import org.junit.jupiter.api.Test
import java.net.URI
import kotlin.test.assertEquals

class RfcOauthClientTest {
  @Test
  fun getAuthorizationUri() {
    val client = RfcOauthClient(
      URI("https://eternal-twin.net/oauth/authorize"),
      URI("https://eternal-twin.net/oauth/token"),
      URI("https://mygame.localhost/oauth/callback"),
      "mygame@clients",
      "dev_secret",
    )

    val actual: URI = client.getAuthorizationUri("base", "authenticate");
    val expected: URI = URI("https://eternal-twin.net/oauth/authorize?access_type=offline&response_type=code&redirect_uri=https%3A%2F%2Fmygame.localhost%2Foauth%2Fcallback&client_id=mygame%40clients&scope=base&state=authenticate")
    assertEquals(expected, actual)
  }

  @Test
  fun getAuthorizationUriNamed() {
    val client = RfcOauthClient(
      authorizationEndpoint = URI("https://eternal-twin.net/oauth/authorize"),
      tokenEndpoint = URI("https://eternal-twin.net/oauth/token"),
      callbackEndpoint = URI("https://mygame.localhost/oauth/callback"),
      clientId = "mygame@clients",
      clientSecret = "dev_secret",
    )

    val actual: URI = client.getAuthorizationUri("base", "authenticate");
    val expected: URI = URI("https://eternal-twin.net/oauth/authorize?access_type=offline&response_type=code&redirect_uri=https%3A%2F%2Fmygame.localhost%2Foauth%2Fcallback&client_id=mygame%40clients&scope=base&state=authenticate")
    assertEquals(expected, actual)
  }
}
