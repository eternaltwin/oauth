# OAuth client

[![Bintray](https://img.shields.io/bintray/v/eternal-twin/maven/oauth-client)][bintray]

Official Eternal-Twin OAuth client for Java and Kotlin.

## Tasks

- `./gradlew :test`: Run the test suite
- `./gradlew :dokkaHtml`: Generate the documentation
- `./gradlew :publish`: Publish the new library version

[bintray]: https://bintray.com/eternal-twin/maven/oauth-client
